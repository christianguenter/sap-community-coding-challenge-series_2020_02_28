*&---------------------------------------------------------------------*
*& Report zabap_challenge
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT zabap_challenge.

cl_demo_output=>display( CONV string(
                          LET sentence = `ABАP  is excellent `
                              match_word_regex = `\w+`
                              match_duplicate_chars_regex = `(.)(?=.*\1)`
                              num_of_words = count( val   = sentence
                                                    regex = match_word_regex )
                          IN REDUCE #( INIT output = |Number of words: { num_of_words }|
                                       FOR word_index = 1 WHILE word_index <= num_of_words
                                       LET word = match( val   = sentence
                                                         regex = match_word_regex
                                                         occ   = word_index )
                                           num_of_unique_chars = numofchar( replace( val   = word
                                                                                     regex = match_duplicate_chars_regex
                                                                                     occ   = 0
                                                                                     with  = space ) )
                                       IN NEXT output = output && |\nNumber of unique characters in word: { word } - { num_of_unique_chars }| ) ) ).
